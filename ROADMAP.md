# Planned Features

# Labels

- 🚧️ : In progress
- ✔️: Done waiting for release
- 💤️ : Waiting for development

# Scheduled for 2.3.0

- TO BE DETERMINED

# 2.2.0

- ✔️custom-system-builder#82 Added Drag&Drop on components to allow movement everywhere

# Planned

- 💤️ custom-system-builder#5 - Ability to add items to other items
- 💤️ custom-system-builder#158 - Creating packages to assign multiple items and numeric modifiers